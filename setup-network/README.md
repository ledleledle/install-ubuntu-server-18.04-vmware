## Setup Static Network Ubuntu Server 18.04 di dalam VMWare

Sebelumnya saya sudah melakukan <a href="https://gitlab.com/ledleledle/install-ubuntu-server-18.04-vmware/-/tree/master/install-ubuntu-server">installasi Ubuntu Server 18.04</a>. Namun sebelumnya saya menggunakan ip DHCP. Sedangkan saat kita membutuhkan server yang dapat **diakses** kapan saja tanpa harus berubah-ubah IP, kita membutuhkan ip static.

#### Local PC
- Disarankan untuk melihat ketersedian ip pada PC local terlebih dahulu, agar tidak terjadi bentrok antar ip. Gunakan perintah <code>arp</code> pada terminal local PC.

![alt text](ss/1.png "SS-1")

- Ubah pengaturan network VM dari NAT ke Bridged. <code>Klik kanan pada machine > Virtual Machine Settings > Network Adapter > Ubah ke Bridged</code>
![alt text](ss/bridged.png "SS-Bridged")

#### Virtual Machine
- Start VM lalu edit file <code>/etc/netplan/00-installer-config.yaml</code> dengan text editor favorit (contoh vim).
```
sudo vim /etc/netplan/00-installer-config.yaml
```
![alt text](ss/2.png "SS-2")

- Tambahkan beberapa baris kode, dan edit <code>dhcp4: true</code> menjadi <code>no</code>
![alt text](ss/3.png "SS-3")

- Simpan & keluar dari text editor <code>:wq</code> (untuk vim). Lalu masukkan perintah dibawah ini untuk mengkonfirmasi perubahan yang sudah dilakukan.
```
sudo netplan apply
```

- Coba test, apakah vm terhubung ke internet atau tidak.
![alt text](ss/4.png "SS-4")

### Bonus
Well... Sukses, sekarang saya dapat mengaksesnya lewat ssh, dan saya juga bisa memasang web server pada virtual machine ini untuk diakses melalui komputer lokal saya.

- Masuk melalui SSH
```
ssh leon@192.168.43.199
```
![alt text](ss/5.png "SS-5")

- Install <code>nginx server</code> & aktifkan service.
```
sudo apt install nginx
sudo systemctl start nginx
```

- Buka browser pada komputer local dan masukkan ip virutal machine.
![alt text](ss/6.png "SS-6")