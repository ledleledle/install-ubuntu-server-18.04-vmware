# Install Ubuntu Server 18.04 VMWare

### Week 1
#### Install Ubuntu Server 18.04 VMWare, mulai dari setup, network, segala macem

# Daftar Isi
- <a href="https://gitlab.com/ledleledle/install-ubuntu-server-18.04-vmware/-/tree/master/install-ubuntu-server">Installasi Ubuntu Server 18.04 dengan VMWare</a>
- <a href="https://gitlab.com/ledleledle/install-ubuntu-server-18.04-vmware/-/tree/master/setup-network">Setup Network</a>
- <a href="https://gitlab.com/ledleledle/install-ubuntu-server-18.04-vmware/-/tree/master/install-app-nodejs">Installasi Aplikasi Node.js</a>

## Special thanks for
- Bootcamp Dumbways.id
- Temen" bootcamp
- Dan mentor" tercinta ❤️