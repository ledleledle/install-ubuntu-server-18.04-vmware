## Installasi Ubuntu Server 18.04 di dalam VMWare

- Buat virtual server baru, beri nama, user & password sesuai kebutuhan, ini digunakan untuk inisialisai OS Ubuntu Server.
![alt text](ss/ss-14.png "SS-1")

- Beri nama VM yang akan digunakan.
![alt text](ss/ss-13.png "SS-2")

Beri virtual server, spesifikasi yang sesuai kebutuhan. Setup yang digunakan pada virtual machine saya yaitu :

| Item    | Value                 |
| ------- | --------------------- |
| RAM     | 1GB                   |
| CPU     | 1 Core                |
| Storage | 5GB                   |
| Network | NAT (Sementara)       |

- Lalu, jalankan VM. Pilih bahasa, ini akan dijadikan patokan bahasa untuk seterusnya.
![alt text](ss/ss-12.png "SS-3")

- Pilih layout keyboard. Untuk keymap yang digunakan di negara saya (Indonesia), kami menggunakan keymap **English**.
![alt text](ss/ss-11.png "SS-4")

- Untuk network, saya menggunakan DHCP terlebih dahulu, karena untuk ip static akan dikupas di pembahasan berikutnya.
![alt text](ss/ss-10.png "SS-5")

- Setelah itu, tekan enter (Done) sampai pada proses partisi hardisk.

- Pilih **/dev/sda**, lalu pilih menu **Add GPT partition**
![alt text](ss/ss-9.png "SS-6")
- Untuk partisi swap, gunakan format **swap**
![alt text](ss/ss-8.png "SS-7")
- Dan untuk partisi sistem (/). Gunakan format **ext4**, dengan mount point (/).
![alt text](ss/ss-7.png "SS-8")

Hasil akhir partisi yaitu :

| Item    | Value                 |
| ------- | --------------------- |
| Swap    | 500M                  |
| /       | 4.5G                  |

- Isi identitas untuk server.
![alt text](ss/ss-6.png "SS-9")

- Install OpenSSH (mungkin akan dibutuhkan dipraktek selanjutnya).
![alt text](ss/ss-5.png "SS-10")

- Tekan enter (Done) hingga proses installasi berjalan. Siapkan gorengan dan kopi, karena biasanya akan memakan waktu yang lama.
![alt text](ss/ss-4.png "SS-11")

- Setelah proses installasi berhasil, maka installer memberikan perintah untuk melakukan **reboot** pada VM. Setelah reboot, akan muncul tampilan login, masukkan user dan password yang sudah dibuat sebelumnya. Lalu jika login berhasil maka akan muncul tampilan seperti dibawah ini.
![alt text](ss/ss-3.png "SS-12")

- Untuk koneksi internet, bisa dicek dengan perintah **ping**
![alt text](ss/ss-2.png "SS-13")