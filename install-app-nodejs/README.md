## Install & run app Node.js di dalam VMWare

Sebelumnya saya sudah melakukan <a href="https://gitlab.com/ledleledle/install-ubuntu-server-18.04-vmware/-/tree/master/setup-network">Setup IP Static</a>. Pada task kali ini, saya akan setup VM saya menjadi server **Node.JS**.

Saya praktek menggunakan SSH agar memudahkan saya dalam copy & paste beberapa link, nyalakan VM terlebih dahulu sebelum mengaksesnya melalui SSH.
```
ssh leon@192.168.43.199
```
![alt text](0.png "SS-SSH")

- Install Node.JS versi 10.x
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

- Clone project **Node.JS** yang telah disediakan
```
git clone https://github.com/sgnd/dumbplay.git
```

- Masuk ke folder <code>dumbplay/frontend</code>
```
cd dumbplay/frontend
```

- Install paket dari <code>package.json</code>
```
npm install
```

- Start Node.JS application
```
npm run start

#atau

npm start
```

### Result
![alt text](1.png "SS-1")